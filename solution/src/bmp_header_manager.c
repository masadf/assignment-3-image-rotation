#include <bmp_header_manager.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint8_t calculate_padding(uint32_t width) {
    return 4 - width * PIXEL_SIZE % 4;
}

bool read_bmp_header(FILE *file, struct bmp_header *header) {
    return fread(header, BMP_HEADER_SIZE, 1, file);
}

bool write_bmp_header(FILE *file, struct bmp_header *header) {
    return fwrite(header, BMP_HEADER_SIZE, 1, file);
}

struct bmp_header create_bmp_header(struct image *image) {
    uint32_t width = image->width;
    uint32_t height = image->height;
    uint8_t padding = calculate_padding(width);
    struct bmp_header header = {0};

    header.biWidth = width;
    header.biHeight = height;
    header.bfileSize = BMP_HEADER_SIZE + height * (width + padding) * PIXEL_SIZE;
    header.biSizeImage = width * height * PIXEL_SIZE;
    header.bfType = BMP_TYPE;
    header.bfReserved = 0;
    header.bOffBits = BMP_HEADER_SIZE;
    header.biSize = INFO_HEADERS_SIZE;
    header.biPlanes = PLANES;
    header.biBitCount = BIT_COUNT;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}



