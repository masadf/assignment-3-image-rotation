#include "bmp_header_manager.h"
#include "save-load//bmp_file_manager.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("You don't give important parameters");
        return 1;
    }

    FILE *input_file;
    if (open_for_read(&input_file, argv[1]) != READ_OK) {
        printf("Reading error!");
        close(input_file);
        return 1;
    };
    FILE *output_file;
    if (open_for_write(&output_file, argv[2]) != WRITE_OK) {
        printf("Creation file error!");
        close(input_file);
        close(output_file);
        return 1;
    };

    struct image input_image;
    if (load_bmp(input_file, &input_image) != READ_OK) {
        printf("BMP header error!");
        close(input_file);
        close(output_file);
        return 1;
    }

    struct image output_image = {0};
    if (get_rotated_image(&input_image, &output_image) != EXECUTION_OK) {
        printf("Image rotating error!");
        close(input_file);
        close(output_file);
        return 1;
    }

    if (save_bmp(output_file, &output_image) != WRITE_OK) {
        printf("Image writing error!");
        close(input_file);
        close(output_file);
        return 1;
    }

    close(input_file);
    close(output_file);

    free(input_image.data);
    free(output_image.data);
    return 0;
}
