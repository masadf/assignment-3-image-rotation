#include <bmp_header_manager.h>
#include <stdlib.h>

enum file_writing_status construct_image(struct image *image, uint32_t height, uint32_t width) {
    image->height = height;
    image->width = width;
    image->data = malloc(height * width * PIXEL_SIZE);
    if (image->data == NULL) {
        return WRITE_FAILED;
    }
    return WRITE_OK;
}

enum execution_status get_rotated_image_structure(struct image *image, struct image *rotated_image) {
    if (construct_image(rotated_image, image->width, image->height) != WRITE_OK) {
        return EXECUTION_FAILED;
    }

    return EXECUTION_OK;
}

uint32_t calculate_address_after_rotate(struct image *image, uint32_t i, uint32_t j) {
    return image->height - i - 1 + j * image->height;
}

enum execution_status get_rotated_image(struct image *image, struct image *rotated_image) {
    if (get_rotated_image_structure(image, rotated_image) != EXECUTION_OK) {
        return EXECUTION_FAILED;
    }

    for (uint32_t i = 0; i < image->height; i++) {
        for (uint32_t j = 0; j < image->width; j++) {
            uint32_t new_address = calculate_address_after_rotate(image, i, j);
            rotated_image->data[new_address] = image->data[i * image->width + j];
        }
    }

    return EXECUTION_OK;
}

