#include "save-load/file-manager.h"
#include <stdio.h>

enum file_reading_status open_for_read(FILE **file, char *path) {
    *file = fopen(path, "rb");
    if (file == NULL){
        return READ_FAILED;
    }
    return READ_OK;
}

enum file_writing_status open_for_write(FILE **file, char *path) {
    *file = fopen(path, "wb");
    if (file == NULL){
        return WRITE_FAILED;
    }
    return WRITE_OK;
}

void close(FILE *file) {
    fclose(file);
}
