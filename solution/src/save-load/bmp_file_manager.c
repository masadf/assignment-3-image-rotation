#include <bmp_header_manager.h>
#include <stdlib.h>

enum file_reading_status load_bmp(FILE *file, struct image *image) {
    struct bmp_header header;
    if (!read_bmp_header(file, &header)) {
        return READ_INVALID_HEADER;
    }
    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;

    if (construct_image(image, height, width) != WRITE_OK) {
        return READ_FAILED;
    }

    uint8_t padding = calculate_padding(image->width);

    for (size_t i = 0; i < height; i++) {
        struct pixel *start_row_pixel = &(image->data[i * width]);
        if (!fread(start_row_pixel, PIXEL_SIZE, width, file)) {
            free(image->data);
            return BMP_ERROR;
        }
        if (fseek(file, padding, SEEK_CUR)) {
            free(image->data);
            return BMP_ERROR;
        }
    }
    return READ_OK;
}

enum file_writing_status end_row_by_padding(FILE *file, uint8_t padding) {
    const struct pixel zero[3] = {{0},
                                  {0},
                                  {0}};
    if (!fwrite(zero, 1, padding, file)) {
        return WRITE_FAILED;
    }
    return WRITE_OK;
}

enum file_writing_status save_bmp(FILE *file, struct image *image) {
    uint32_t width = image->width;
    uint32_t height = image->height;
    struct bmp_header header = create_bmp_header(image);
    if (!write_bmp_header(file, &header)) {
        return WRITE_FAILED;
    }

    uint8_t padding = calculate_padding(image->width);

    for (size_t i = 0; i < height; i++) {
        struct pixel *start_row_pixel = &(image->data[i * width]);
        if (!fwrite(start_row_pixel, PIXEL_SIZE, width, file)) {
            free(image->data);
            return WRITE_FAILED;
        }
        if (end_row_by_padding(file, padding) != WRITE_OK) {
            free(image->data);
            return WRITE_FAILED;
        }
    }
    return WRITE_OK;
}



