#ifndef IMAGE_TRANSFORMER_BMP_HEADER_MANAGER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_MANAGER_H

#include <image_manager.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define BMP_TYPE 0x4d42
#define PLANES 1
#define BIT_COUNT 24
#define BMP_HEADER_SIZE 54
#define INFO_HEADERS_SIZE 40
#define PIXEL_SIZE 3

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

bool write_bmp_header(FILE *file, struct bmp_header *header);

bool read_bmp_header(FILE *file, struct bmp_header *header);

uint8_t calculate_padding(uint32_t width);

struct bmp_header create_bmp_header(struct image *image);

#endif //IMAGE_TRANSFORMER_BMP_HEADER_MANAGER_H
