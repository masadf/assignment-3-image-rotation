#ifndef IMAGE_TRANSFORMER_IMAGE_MANAGER_H
#define IMAGE_TRANSFORMER_IMAGE_MANAGER_H

#include <save-load/file-manager.h>
#include <stdint.h>

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

enum file_writing_status construct_image(struct image *image, uint32_t height, uint32_t width);

enum execution_status get_rotated_image(struct image *image, struct image *rotated_image);

#endif //IMAGE_TRANSFORMER_IMAGE_MANAGER_H
