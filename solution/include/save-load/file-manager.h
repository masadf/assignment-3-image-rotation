#ifndef IMAGE_TRANSFORMER_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_FILE_MANAGER_H

#include <stdio.h>

enum file_reading_status {
    READ_OK,
    READ_INVALID_HEADER,
    BMP_ERROR,
    READ_FAILED
};

enum file_writing_status {
    WRITE_OK,
    WRITE_FAILED
};

enum execution_status {
    EXECUTION_OK,
    EXECUTION_FAILED
};

enum file_reading_status open_for_read(FILE **file, char *path);

enum file_writing_status open_for_write(FILE **file, char *path);

void close(FILE *file);

#endif //IMAGE_TRANSFORMER_FILE_MANAGER_H
