#ifndef IMAGE_TRANSFORMER_BMP_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_BMP_FILE_MANAGER_H

#include <save-load/file-manager.h>

enum file_reading_status load_bmp(FILE *file, struct image *image);

enum file_writing_status save_bmp(FILE *file, struct image *image);

#endif //IMAGE_TRANSFORMER_BMP_FILE_MANAGER_H
