#include "cmp.h"
#include <stdio.h>

enum cmp_result file_cmp(FILE *f1, FILE *f2, size_t sz);
